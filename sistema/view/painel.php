<?php
    session_start();
    include('../classe/verifica_login.php');
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Sistema de Pedidos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
        <!-- BootstrapCDN para fornecer a versão em cache do CSS e JS compilados do Bootstrap para o/ projeto. -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <style>
            body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
            body {font-size:16px;}
            .w3-half img{margin-bottom:-6px;margin-top:16px;opacity:0.8;cursor:pointer}
            .w3-half img:hover{opacity:1}
        </style>
    </head>
    <body>
        <!-- Menu -->
        <nav class="w3-sidebar w3-red w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
            <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px">Fechar Menu</a>
            <div class="w3-container">
                <h3 class="w3-padding-64"><b>Sistema de<br>Pedidos</b></h3>
            </div>
            <div class="w3-bar-block">
                <a href="#" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Inicio</a>
                <a href="./clientes.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Clientes</a>
                <a href="./produto.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Produtos</a>
                <a href="./pedido.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Pedidos</a>
                <a href="./itens_pedido.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Itens Pedido</a>
                <a href="../index.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Sair</a>
            </div>
        </nav>

        <!-- Top Menu Responsivo para diferentes telas -->
        <header class="w3-container w3-top w3-hide-large w3-red w3-xlarge w3-padding">
            <a href="javascript:void(0)" class="w3-button w3-red w3-margin-right" onclick="w3_open()">☰</a>
            <span>Sistema de Pedidos</span>
        </header>

        <!-- Efeito de sobreposição ao abrir a barra lateral -->
        <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="Fechar Menu" id="myOverlay"></div>

        <!-- !Conteúdo da página! -->
        <div class="w3-main" style="margin-left:340px;margin-right:40px">
            <!-- Listar Pedidos -->
            <div class="w3-container" id="pedidos" style="margin-top:75px">
                <h1 class="w3-xxxlarge w3-text-deep-orange"><b>Página Inicial do sistema</b></h1>
                <hr style="width:250px;border:5px solid" class="w3-round w3-text-deep-orange"><br>
                <p>Organização é um dos fatores mais importantes para manter uma bom controle de vendas para um negócio.<br></p>
                <p>Com um sistema de controle de pedidos é possível centralizar todas as informações referentes a seus produtos, usuarios e pedidos. Dessa maneira, terá uma gestão organizacional muito mais eficiente.</p>
                <h1 class="w3-xxxlarge w3-text-deep-orange"><b>Como utilizar?</b></h1>
                <hr style="width:250px;border:5px solid" class="w3-round w3-text-deep-orange"><br>
                <p>O menu ao lado possibilita realizar todas as funções do sistema.</p>
                <ul> 
                    <li>Listar, adicionar e remover Clientes/Usuários!</li> 
                    <li>Listar, adicionar e remover Produtos!</li> 
                    <li>Listar, adicionar e remover Pedidos!</li> 
                </ul>
            </div>
        </div>
        <!-- Fim do Conteúdo da página -->

        <script>
        // Script para abrir e fechar a barra lateral
        function w3_open() {
          document.getElementById("mySidebar").style.display = "block";
          document.getElementById("myOverlay").style.display = "block";
        }
         
        function w3_close() {
          document.getElementById("mySidebar").style.display = "none";
          document.getElementById("myOverlay").style.display = "none";
        }
        </script>
    </body>
</html>
