<?php
    session_start();
    include("../classe/conexao.php");

    $consult = "SELECT usuario_id, nome,usuario,data_cadastro FROM login.usuario";
    $result = mysqli_query($conexao, $consult);
?>

<!DOCTYPE html>
<html lang="pt-br">
    <header>
        <title>Sistema de Pedidos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
        <!-- BootstrapCDN para fornecer a versão em cache do CSS e JS compilados do Bootstrap para o/ projeto. -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <style>
            body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
            body {font-size:16px;}
            .w3-half img{margin-bottom:-6px;margin-top:16px;opacity:0.8;cursor:pointer}
            .w3-half img:hover{opacity:1}
        </style>
    </header>
    <body>
        <!-- Menu -->
        <nav class="w3-sidebar w3-red w3-collapse w3-top w3-large w3-padding" style="z-index:3;width:300px;font-weight:bold;" id="mySidebar"><br>
          <a href="javascript:void(0)" onclick="w3_close()" class="w3-button w3-hide-large w3-display-topleft" style="width:100%;font-size:22px">Fechar Menu</a>
          <div class="w3-container">
            <h3 class="w3-padding-64"><b>Sistema de<br>Pedidos</b></h3>
          </div>
          <div class="w3-bar-block">
            <a href="./painel.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Inicio</a>
            <a href="#" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Clientes</a>
            <a href="./produto.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Produtos</a>
            <a href="./pedido.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Pedidos</a>
            <a href="./itens_pedido.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Itens Pedido</a>
            <a href="../index.php" onclick="w3_close()" class="w3-bar-item w3-button w3-hover-white">Sair</a>
          </div>
        </nav>

        <!-- Top Menu Responsivo para diferentes telas -->
        <header class="w3-container w3-top w3-hide-large w3-red w3-xlarge w3-padding">
          <a href="javascript:void(0)" class="w3-button w3-red w3-margin-right" onclick="w3_open()">☰</a>
          <span>Sistema de Pedidos</span>
        </header>

        <!-- Efeito de sobreposição ao abrir a barra lateral -->
        <div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="Fechar Menu" id="myOverlay"></div>
        
        <!-- !Conteúdo da página! -->
        <div class="w3-main" style="margin-left:340px;margin-right:40px">
            <!-- Cadastro de novo Clientes -->
            <div class="w3-container" id="novo_cliente" style="margin-top:75px">
                <h1 class="w3-xxxlarge w3-text-deep-orange"><b>Cadastrar Novo Cliente</b></h1>
                <hr style="width:250px;border:5px solid" class="w3-round w3-text-deep-orange">
                   <p>Preencha as informações do Novo Cliente</p>
                <form action="../classe/cadastrar.php" method="POST">
                    <div class="w3-section">
                        <div class="control">
                            <input name="nome" type="text" class="w3-input w3-border" placeholder="Nome" autofocus required>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="usuario" type="text" class="w3-input w3-border" placeholder="Usuário" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="senha" class="w3-input w3-border" type="password" placeholder="Senha" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="telefone" type="text" class="w3-input w3-border" placeholder="Telefone" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="email" type="text" class="w3-input w3-border" placeholder="E-mail" autofocus required>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="data_nac" type="text" class="w3-input w3-border" placeholder="Data de Nascimento" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="genero" type="text" class="w3-input w3-border" placeholder="Gênero" autofocus>
                        </div>
                    </div>
                      <button type="submit" class="w3-block w3-padding-large w3-green w3-margin-bottom">Cadastrar Cliente</button>
                </form> 
              </div>
        </div>
        <!-- Listar Clientes -->
        <div class="w3-main" style="margin-left:340px;margin-right:40px">
            <div class="w3-container" id="cliente" style="margin-top:75px">
                <h1 class="w3-xxxlarge w3-text-deep-orange"><b>Lista de Clientes</b></h1>
                <hr style="width:250px;border:5px solid" class="w3-round w3-text-deep-orange">
                <div class="table-responsive">        
                    <table border="1" class="table table-sm table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <td scope="col">ID</td>
                                <td scope="col">Nome</td>
                                <td scope="col">Usuario</td>
                                <td scope="col">Data do cadastro</td>
                            </tr>
                        </thead>
                        <?php while($dado = mysqli_fetch_array($result)){ ?>
                               <tr>
                                <td scope="row"><?php echo $dado["usuario_id"]?></td>
                                <td scope="row"><?php echo $dado["nome"]?></td>
                                <td scope="row"><?php echo $dado["usuario"]?></td>
                                <td scope="row"><?php echo date("d/m/Y", strtotime($dado["data_cadastro"]))?></td>
                                <td scope="row"><!--<a href="usu_editar.php?usuario=<?php echo $dado["usuario"]?>">Editar</a>-->
                                    <a href="javascript: if(confirm('Tem certexa que deseja deletar o usuário <?php echo $dado["nome"];?>?'))
                                    location.href='../classe/deletar_usu.php?usuario_id=<?php echo $dado["usuario_id"]?>';"><button type="button" class="btn btn-xs btn-danger">Excluir</button></a></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
             </div>
        </div>
        <!-- Editar Clientes -->
        <div class="w3-main" style="margin-left:340px;margin-right:40px">
            <div class="w3-container" id="novo_cliente" style="margin-top:75px">
                <h1 class="w3-xxxlarge w3-text-deep-orange"><b>Editar Informações do Cliente</b></h1>
                <hr style="width:250px;border:5px solid" class="w3-round w3-text-deep-orange">
                   <p>Preencha as informações para atualização do Cliente</p>
                <form action="../classe/alterar_cliente.php" method="POST">
                    <div class="w3-section">
                        <div class="control">
                            <input name="id" type="text" class="w3-input w3-border" placeholder="Informe o ID que deseja editar" autofocus required>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="nome" type="text" class="w3-input w3-border" placeholder="Nome" autofocus required>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="usuario" type="text" class="w3-input w3-border" placeholder="Usuário" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="senha" class="w3-input w3-border" type="password" placeholder="Senha" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="telefone" type="text" class="w3-input w3-border" placeholder="Telefone" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="email" type="text" class="w3-input w3-border" placeholder="E-mail" autofocus required>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="data_nasc" type="text" class="w3-input w3-border" placeholder="Data de Nascimento" autofocus>
                        </div>
                    </div>
                    <div class="w3-section">
                        <div class="control">
                            <input name="genero" type="text" class="w3-input w3-border" placeholder="Gênero" autofocus>
                        </div>
                    </div>
                      <button type="submit" class="w3-block w3-padding-large w3-green w3-margin-bottom">Atualizar Cliente</button>
                </form>
              </div>
        </div>
        <!-- Fim do Conteúdo da página -->
        <script>
        // Script para abrir e fechar a barra lateral
        function w3_open() {
            document.getElementById("mySidebar").style.display = "block";
            document.getElementById("myOverlay").style.display = "block";
        }
         
        function w3_close() {
            document.getElementById("mySidebar").style.display = "none";
            document.getElementById("myOverlay").style.display = "none";
        }
        </script>
    </body>
</html>