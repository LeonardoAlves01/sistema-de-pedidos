<?php
    include("../classe/conexao.php");

    $prod_codigo = intval($_GET['produto_id']);
    //SQL para deletar produto
    $sql_code = "DELETE FROM produtos WHERE produto_id = '$prod_codigo'";
    $sql_query = mysqli_query($conexao, $sql_code) or die($mysqli->error);

    if ($sql_query)
        echo "
        <script>
            alert('O Produto foi deletado com sucesso.');
            location.href='../view/produto.php';
        </script>";
    else
        echo "
        <script>
            alert('Não foi possível deletar o Produto.');
            location.href='../view/produto.php';
        </script>";
