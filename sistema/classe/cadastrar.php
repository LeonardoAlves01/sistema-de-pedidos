<?php
    session_start();
    include("../classe/conexao.php");

    $nome = mysqli_real_escape_string($conexao, trim($_POST['nome']));
    $usuario = mysqli_real_escape_string($conexao, trim($_POST['usuario']));
    $senha = mysqli_real_escape_string($conexao, trim(md5($_POST['senha'])));
    $telefone = mysqli_real_escape_string($conexao, trim(($_POST['telefone'])));
    $email = mysqli_real_escape_string($conexao, trim(($_POST['email'])));
    $data_nasc = mysqli_real_escape_string($conexao, trim(($_POST['data_nasc'])));
    $genero = mysqli_real_escape_string($conexao, trim(($_POST['genero'])));

    $sql = "select count(*) as total from usuario where usuario = '$usuario'";
    $result = mysqli_query($conexao, $sql);
    $row = mysqli_fetch_assoc($result);
    //Verificação se já existe o usuario que está sendo cadastrado
    if ($row['total'] == 1) {
        $_SESSION['usuario_existe'] = true;
        header('Location: ./view/cadastro.php');
        exit;
    }
    //Inserir dados do novo usuario na tabela de usuarios
    $sql = "INSERT INTO usuario
            (nome, usuario, senha, data_cadastro, telefone, email, data_nasc, genero)
            VALUES
            ('$nome', '$usuario', '$senha', NOW(), '$telefone', '$email', '$data_nasc', '$genero')";

    if ($conexao->query($sql) === true) {
        $_SESSION['status_cadastro'] = true;
    }

    $conexao->close();

    header('Location: ../index.php');
    exit;
