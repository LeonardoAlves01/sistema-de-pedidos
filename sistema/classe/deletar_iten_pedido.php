<?php
    include("../classe/conexao.php");

    $id_item_pedido = intval($_GET['id_item_pedido']);
    //SQL para deletar item pedido
    $sql_code = "DELETE FROM item_pedido WHERE id_item_pedido = '$id_item_pedido'";
    $sql_query = mysqli_query($conexao, $sql_code) or die($mysqli->error);

    if ($sql_query)
        echo "
        <script>
            alert('O Pedido foi deletado com sucesso.');
            location.href='../view/itens_pedido.php';
        </script>";
    else
        echo "
        <script> 
            alert('Não foi possível deletar o Pedido.');
            location.href='../view/itens_pedido.php';
        </script>";
