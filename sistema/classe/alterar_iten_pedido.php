<?php
    session_start();
    include("../classe/conexao.php");

    $id_item_pedido = mysqli_real_escape_string($conexao, trim($_POST['id_item_pedido']));
    $quant_prod = mysqli_real_escape_string($conexao, trim($_POST['quant_prod']));
    
    $sql_code = "SELECT valor FROM item_pedido WHERE id_item_pedido = '$id_item_pedido'";
    $sql_query = mysqli_query($conexao, $sql_code) or die($mysqli->error);
    $row = mysqli_fetch_array($sql_query);
    $valor_total = $row['valor'] *  $quant_prod;
    //Alteração dos dados do item_pedidos cadastrado
    $sql = "UPDATE item_pedido
            SET quant_produto = '$quant_prod', valor_total = $valor_total
            WHERE id_item_pedido = '$id_item_pedido'";
    
    if ($conexao->query($sql) === true) {
        $_SESSION['status_alteracao'] = true;
    }

    $conexao->close();

    header('Location: ../view/itens_pedido.php');

    exit;
