<?php
    include("../classe/conexao.php");

    $usu_codigo = intval($_GET['usuario_id']);
    //SQL para deletar usuario
    $sql_code = "DELETE FROM usuario WHERE usuario_id = '$usu_codigo'";
    $sql_query = mysqli_query($conexao, $sql_code) or die($mysqli->error);

    if ($sql_query)
        echo "
        <script>
            alert('O usuário foi deletado com sucesso.');
            location.href='../view/clientes.php';
        </script>";
    else
        echo "
        <script>
            alert('Não foi possível deletar o usuário.');
            location.href='../view/clientes.php';
        </script>";
