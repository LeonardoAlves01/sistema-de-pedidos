﻿# Sistema-de-Pedidos
Organização é um dos fatores mais importantes para manter uma bom controle de vendas para um negócio. Com um  sistema de controle de pedidos é possível centralizar todas as informações referentes a seus produtos, usuarios e pedidos. Dessa maneira, terá uma gestão organizacional muito mais eficiente.

O sistema possui um controle de cadastro de usuários, o qual é utilizado para realizar o login. As páginas estão divididas entre clientes, produtos, pedidos e itens pedido. Todas estas podendo ser feita a inserção de novos dados, exclusão e modificação. 

# Quais aplicações possíveis?
Pode ser utilizado para gerenciar qualquer tipo de empresa de vendas, porém será necessários fazer modificações tanto no front-end quanto back-end, para se adequar a casos específicos.

# Qual o motivo do desenvolvimento?
O sistema de pedidos foi desenvolvido como forma de teste de seleção durante o processo de entrevista para vaga de desenvolvedor.

# Discussões
Todo o código está dilponível e aberto para discussões. Se possível rotule a discussão com um tipo apropriado: Bug, Docs, Dúvida, Melhoria, Função, Manutenção, ou DevTools.

# Pré-requisitos
Servidor (utilizado: XAMPP 7.2.14)
PHP 7.2.14 +
Mysql  5.7 +